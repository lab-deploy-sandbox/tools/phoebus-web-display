## Display Builder Web Runtime
# 1.  To change the default EPICS_CA_ADDR_LIST and EPICS_CA_MAX_ARRAY_BYTES
#     see stage main at end
# 2.  Block text are stages
# 3.  Each stage can be tagged and previous stages run from an image cache for rapid developemnt. 
#     For example, to rebuild pvenv with an exisitng image, myimage/dbwr:latest
#       $ docker build  --target pvenv --cache-from myimage/dbwr:latest -t pvws/pvenv .
#     stages that are ahead will then need to be rebuilt:
#       $ docker build  --target pvenv --cache-from pvws/pvenv .

##############
#   Base     #
##############
FROM centos:centos7 AS base
MAINTAINER Rory Clarke
#
WORKDIR /home/PhoebusDisplayBuilder/
#
# Load tools
RUN yum install -y wget git vim sudo java-11-openjdk-devel
#
# Load ant
RUN wget https://apache.mirrors.nublue.co.uk//ant/binaries/apache-ant-1.9.15-bin.tar.gz
RUN tar -xzsf apache-ant-1.9.15-bin.tar.gz;  mv apache-ant-1.9.15/ ./ant
RUN wget https://apache.mirrors.nublue.co.uk/tomcat/tomcat-8/v8.5.63/bin/apache-tomcat-8.5.63.tar.gz
RUN tar -xzsf apache-tomcat-8.5.63.tar.gz;  mv apache-tomcat-8.5.63/ ./tomcat

##############
#   Env      #
##############
FROM base AS env
WORKDIR /home/PhoebusDisplayBuilder/
ENV MYHOME=/home/PhoebusDisplayBuilder
ENV ANT_HOME=$MYHOME/ant
ENV CATALINA_HOME=$MYHOME/tomcat
ENV JAVA_HOME=/usr/lib/jvm/java-openjdk/
ENV PATH=$JAVA_HOME:$CATALINA_HOME/:$ANT_HOME/bin:$PATH
RUN chmod +x $CATALINA_HOME/bin/startup.sh;   \
    chmod +x $CATALINA_HOME/bin/shutdown.sh;  \
    chmod +x $CATALINA_HOME/bin/catalina.sh;

##############
#   pv       #
##############
FROM env AS pv
WORKDIR /home/PhoebusDisplayBuilder/
RUN git clone https://github.com/kasemir/pvws.git ./pvws
RUN cd ./pvws ; ant clean war; cp pvws.war $CATALINA_HOME/webapps

##############
#   GUI      #
##############
FROM pv AS pvws
WORKDIR /home/PhoebusDisplayBuilder/
RUN git clone https://github.com/kasemir/dbwr.git ./dbwr
RUN cd ./dbwr ; ant clean war; cp dbwr.war $CATALINA_HOME/webapps

##############
#   Run      #
##############
FROM pvws AS main
#
# Set EPICS
RUN echo '#!/usr/bin/env bash'     >  $CATALINA_HOME/bin/setenv.sh; \                                   
echo 'export EPICS_CA_ADDR_LIST="172.30.238.13 192.168.83.255 148.79.255.255"' >> $CATALINA_HOME/bin/setenv.sh;  \
echo 'export EPICS_CA_MAX_ARRAY_BYTES=10000000'                 >> $CATALINA_HOME/bin/setenv.sh;
#
# Run
CMD cd $CATALINA_HOME/bin/; \
    ./startup.sh; \
	watch ps -elf | grep java ; 


